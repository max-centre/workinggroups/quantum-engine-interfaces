The aim of this repository is two-fold:

  * To hold discussions via the "issues" board.
  * To keep examples of code and proofs-of-concept of approaches.
  
  
At this point the repository ("project" in GitLab parlance) is "Private", accessible by
default only to members of the 'max-centre' group.

Code can be added in separate branches pushed to the repository by members. The
'master' branch is protected. Merge requests can of course be made and are welcome.
